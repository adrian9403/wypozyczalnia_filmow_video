using React;
using JavaScriptEngineSwitcher.Core;
using JavaScriptEngineSwitcher.V8;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(video_rental_store.ReactConfig), "Configure")]

namespace video_rental_store
{
	public static class ReactConfig
	{
		public static void Configure()
		{
          ReactSiteConfiguration.Configuration
              .AddScript("~/Scripts/components/Sample.jsx");

          JsEngineSwitcher.Current.DefaultEngineName = V8JsEngine.EngineName;
          JsEngineSwitcher.Current.EngineFactories.AddV8();
        }
	}
}