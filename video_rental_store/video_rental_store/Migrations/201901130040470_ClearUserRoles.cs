namespace video_rental_store.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClearUserRoles : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM AspNetUserRoles");
        }
        
        public override void Down()
        {
        }
    }
}
