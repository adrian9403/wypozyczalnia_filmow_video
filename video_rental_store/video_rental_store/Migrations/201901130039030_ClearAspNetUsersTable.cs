namespace video_rental_store.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClearAspNetUsersTable : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM AspNetUsers");
        }
        
        public override void Down()
        {
        }
    }
}
