namespace video_rental_store.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetUsers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [DrivingLicense], [Phone]) VALUES (N'362c7cbf-33d9-4b83-b1b6-57c103cece65', N'admin@vrs.com', 0, N'AMx7E5BNtkH0TFvOSCWngwmzIMuBRB1aPlN/JN56VjctiWPyYYsHweC9hACIaqXMbw==', N'157e0226-d3c0-4079-b0fd-0e0e10f74fb6', NULL, 0, 0, NULL, 1, 0, N'admin@vrs.com', N'', N'')");

            Sql("INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [DrivingLicense], [Phone]) VALUES (N'd8808237-e8d1-4ac1-a1bd-bb89b056c3a8', N'guest@vrs.com', 0, N'AOknvTmQ8P+ii/tU5twUZJTIHLQGxopoy9ufIWGC+FlGy020rXLzG86yRKgyYJo6vg==', N'6ac6c96d-4f90-4e0e-9fca-c8ca09dfc506', NULL, 0, 0, NULL, 1, 0, N'guest@vrs.com', N'', N'')");


            Sql("INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'362c7cbf-33d9-4b83-b1b6-57c103cece65', N'dede03d2-1509-40ae-a7a0-806e5a26f786')");

            Sql("INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'362c7cbf-33d9-4b83-b1b6-57c103cece65', N'd7f307b6-5a42-401d-96ef-346c96118e2a')");
        }
        
        public override void Down()
        {
        }
    }
}
