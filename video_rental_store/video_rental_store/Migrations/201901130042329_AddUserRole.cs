namespace video_rental_store.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserRole : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd7f307b6-5a42-401d-96ef-346c96118e2a', N'CanManageCustomers')");
        }
        
        public override void Down()
        {
        }
    }
}
