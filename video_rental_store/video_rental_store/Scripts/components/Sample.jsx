﻿var HelloWorld = React.createClass({
    render: function () {
        return (
            <div>
                <h1> {this.props.name} I am ABC.</h1>
                <h3> 123456789 </h3>
            </div>
        );
    }
});

ReactDOM.render(
    <HelloWorld name="Hello world!" />, document.getElementById('homeContent')
);